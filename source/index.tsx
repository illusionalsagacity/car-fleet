import "./index.css";

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import App from "./App";
import configureStore, { loadCars } from "./configureStore";
import jsonCars from "./exampleCars";

const mount = document.createElement("div");
mount.id = "mount";

const store = configureStore();

// bit contrived but hey it's a coding challenge
store.dispatch(loadCars(jsonCars));

document.body.appendChild(mount);

ReactDOM.createRoot(mount).render(
  <Provider store={store}>
    <App />
  </Provider>,
);
