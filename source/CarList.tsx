import React, { FC } from "react";
import { connect } from "react-redux";
import { RootState } from "./configureStore";

import Car from "./Car";

interface StateProps {
  vins: string[];
}

// each car "listens" to it's own state, but the list only subscribes to the _order_ of the cars.
// this means that so long as the order doesn't change, the entire list won't re-render.
// The way I'm filtering the list keeps the order completely the same, so the list should never re-render unless it gets
// new data.
const CarList: FC<StateProps> = ({ vins }) => {
  return (
    <>
      {vins.map(vin => (
        <Car key={vin} vin={vin} />
      ))}
    </>
  );
};

const mapStateToProps = (state: RootState) => {
  return {
    vins: state.order,
  };
};

export default connect(mapStateToProps)(CarList);
