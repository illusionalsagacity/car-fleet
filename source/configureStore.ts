import { createStore } from "redux";
import createCachedSelector from "re-reselect";
import { createSelector } from "reselect";

import Car from "./entity/car";
import { fromCars } from "./entity/fleetMetrics";
import { assembleCarTrie, find } from "./entity/Trie";

type CarState = {
  searchVin: string | undefined;
  order: string[];
  byVin: { [vin in string]: Car };
};

const initialState: CarState = {
  searchVin: "",
  order: [],
  byVin: {},
};

interface LoadCarsAction {
  type: "LOAD_CARS";
  cars: Car[];
}
export const loadCars = (cars: Car[]): LoadCarsAction => ({ type: "LOAD_CARS", cars });

interface UpdateSearchVinAction {
  type: "SEARCH_VIN";
  vin: string | undefined;
}
export const updateSearchVin = (vin: string | undefined): UpdateSearchVinAction => ({
  type: "SEARCH_VIN",
  vin,
});

export type AnyFunction = (...args: any[]) => any;
// often you don't want to manually type and maintain action signatures in component prop interfaces/types, so this will extract it automatically.
// you don't typically care about the return value of an action creator, as it's just the action, so I have it ignored here.
export type InferActionSignature<T extends AnyFunction> = (...args: Parameters<T>) => void;
export type RootAction = LoadCarsAction | UpdateSearchVinAction;
export type RootState = CarState;

function carReducer(
  state: CarState = initialState,
  action: RootAction,
): CarState {
  switch (action.type) {
    case "SEARCH_VIN":
      return {
        ...state,
        searchVin: action.vin,
      };

    case "LOAD_CARS": {
      const partial = action.cars.reduce(
        (acc, car) => {
          acc.byVin[car.vin] = car;
          acc.order.push(car.vin);
          return acc;
        },
        {
          order: [] as CarState["order"],
          byVin: {} as CarState["byVin"],
        },
      );

      return {
        ...state,
        ...partial,
      };
    }

    default:
      return state;
  }
}

const selectCarState = (state: RootState) => state;

const selectCarsByVinObject = (state: RootState) => selectCarState(state).byVin;

export const selectSearchVin = (state: RootState) => selectCarState(state).searchVin;

export const selectOrder = (state: RootState) => selectCarState(state).order;

const selectVinProp = (_: any, vin: string) => vin;

export const selectCarByVin = (state: RootState, vin: string) => {
  return state.byVin[vin];
};

export const selectFleetMetrics = createSelector(
  selectCarsByVinObject,
  byVin => fromCars(Object.values(byVin)),
);

// memoize trie creation
export const selectTrie = createSelector(
  selectCarsByVinObject,
  byVin => assembleCarTrie(Object.values(byVin) as Car[]),
);

// each index will only be calculated once, per order / vin
export const selectVinIndex = createCachedSelector(
  selectOrder,
  selectVinProp,
  (order, vin) => order.findIndex(x => x === vin),
)(selectVinProp);

export const selectCarIsHidden = createCachedSelector(
  selectTrie,
  selectVinIndex,
  selectSearchVin,
  (trie, index, searchVin) => {
    if (typeof searchVin === "undefined" || searchVin.length === 0) {
      return false
    }

    const filteredIndices = find<number>(searchVin, trie)?.value ?? new Set<number>()

    return !filteredIndices.has(index);
  },
)(selectVinProp);

export default function configureStore() {
  const store = createStore(carReducer);

  return store;
}
