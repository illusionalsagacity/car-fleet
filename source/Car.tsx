import React, { FC } from "react";
import { connect } from "react-redux";

import { RootState, selectCarByVin, selectCarIsHidden } from "./configureStore";
import ICar from "./entity/car";

interface OwnProps {
  vin: string;
}

interface StateProps {
  hidden: boolean;
  car: ICar;
}

type Props = OwnProps & StateProps;

/* I use a hidden prop here so that react can efficiently update large lists. hidden will remove it from being seen,
  but also when concurrent mode comes along may hint to the renderer that it can pre-render this content, so it appears
  automatically when unfiltered
  the reason hidden is so important, is because it keeps the size of the list the same. React is very efficient at adding
  DOM nodes, removing them, but it is fastest when you are just modifying properties on existing elements. This means the
  entire list will never re-render for this example, only individiaul cars. Because they are subscribed to their individual
  car, list items will only ever re-render if that car changes, or gets filtered.
*/
const Car: FC<Props> = ({ car, hidden }) => {
  return (
    <div className="p-4 md:w-1/2 lg:w-1/3 sm:w-full" hidden={hidden}>
      <div className="rounded overflow-hidden shadow-lg bg-white">
        <article className="px-6 py-4">
          <h3 className="font-bold text-xl mb-2">
            {car.year} {car.make} – {car.model}
          </h3>
          <p className="text-sm">
            <span className="block">State: {car.state}</span>
            <span className="block">VIN: {car.vin}</span>
            <span className="block">
              Purchase Price: {`$${car.purchasePrice.toLocaleString()}`}
            </span>
          </p>
        </article>
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState, { vin }: OwnProps) => {
  return {
    hidden: selectCarIsHidden(state, vin),
    car: selectCarByVin(state, vin),
  };
};

export default connect(mapStateToProps)(Car);
