import React, { FC } from "react";

const Navbar: FC = () => {
  return (
    <nav className="navbar">
      <h1 className="inline text-2xl">Car Fleet</h1>
      <a
        className="button"
        href="https://gitlab.com/illusionalsagacity/car-fleet"
      >
        Source Code
      </a>
    </nav>
  );
};

export default Navbar;
