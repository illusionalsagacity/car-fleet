import { hot } from "react-hot-loader/root";
import React, { FC, useTransition } from "react";

import CarList from "./CarList";
import Navbar from "./Navbar";
import { FleetMetrics } from "./entity/fleetMetrics";
import {
  updateSearchVin,
  InferActionSignature,
  RootState,
  selectFleetMetrics,
  selectSearchVin,
} from "./configureStore";
import { connect } from "react-redux";

interface StateProps {
  searchVin: string | undefined;
  metrics: FleetMetrics;
}

interface DispatchProps {
  updateSearchVin: InferActionSignature<typeof updateSearchVin>;
}

type Props = StateProps & DispatchProps;

const App: FC<Props> = ({ searchVin, metrics, updateSearchVin }) => {
  const [startTransition, isPending] = useTransition({ timeoutMs: 100 });
  // const exactMatch: ICar | null = filteredCars.length === 1 ? filteredCars[0] : null;

  return (
    <>
      <Navbar />
      <article className="px-8 pt-4 pb-6 border-b border-gray-300 shadow shadow-2xl">
        <div>
          <label className="block mb-2" htmlFor="VIN">
            Search Car VIN
          </label>
          <input
            className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
            id="VIN"
            type="text"
            onChange={e => {
              startTransition(() => {
                updateSearchVin(e.currentTarget.value.toUpperCase());
              });
            }}
            value={searchVin}
          />
          {/* <div hidden={!exactMatch}>
            <h3 className="font-bold text-xl">Found exact match!</h3>
            {exactMatch && <Car vin={exactMatch.vin} />}
          </div> */}
        </div>
      </article>
      <div className="flex overflow-scroll h-screen">
        <aside className="text-sm text-gray-700 md:w-1/3 sm:w-full">
          <div className="bg-white rounded px-6 py-4 m-8 shadow-lg">
            <span className="block mb-1">
              Total Purchase Price:{" "}
              {`$${metrics.purchaseTotal.toLocaleString()}`}
            </span>
            <span className="block mb-1">Number of Cars: {CarList.length}</span>
            {Object.entries(metrics.countByYear).map(([year, count]) => {
              return (
                <span key={year} className="block mb-1">
                  Number of model year <b>{year}</b> cars: {count}
                </span>
              );
            })}
          </div>
        </aside>
        <main className="m-4 flex flex-rows flex-wrap md:w-2/3 sm:w-full">
          <CarList />
        </main>
      </div>
    </>
  );
};

const mapStateToProps = (state: RootState): StateProps => ({
  searchVin: selectSearchVin(state),
  metrics: selectFleetMetrics(state),
});

const mapDispatchToProps = {
  updateSearchVin,
};

export default hot(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(App),
);
