import Car from "./car";

export interface TrieNode<T> {
  value: Set<T>;
  nodes: Map<string, TrieNode<T>>;
}

export function createNode<T>(value: T[], children: Map<string, TrieNode<T>> = new Map()): TrieNode<T> {
  return {
    value: new Set(value),
    nodes: children,
  };
}

export function insert<T>(
  search: string,
  value: T,
  node: TrieNode<T>,
): TrieNode<T> {
  const [firstChar, ...rest] = search.split("");
  const childSearch = rest.join("");

  node.value.add(value);

  if (rest.length === 0) {
    // we are at the end of the insertion, so set the node
    // mutation is icky but it's convenient for the moment.
    if (!node.nodes.has(firstChar)) {
      node.nodes.set(firstChar, createNode([value]));
    }
    return node;
  }

  const child = node.nodes.get(firstChar) ?? createNode<T>([value]);

  // mutation is icky but it's convenient for the moment.
  node.nodes.set(firstChar, insert(childSearch, value, child));

  return node;
}

export function find<T>(search: string, node: TrieNode<T>): TrieNode<T> | undefined {
  const [firstChar, ...rest] = search.split("");
  const childSearch = rest.join("");

  let matchingNode: TrieNode<T> | undefined = undefined;

  if (typeof firstChar !== "undefined") {
    matchingNode = node.nodes.get(firstChar);
  }

  if (rest.length === 0) {
    return matchingNode;
  } else if (matchingNode !== undefined) {
    return find(childSearch, matchingNode);
  }

  return undefined;
}

export const getPartialMatchIndices = (partial: string, cars: Car[]) => cars.reduce<number[]>((acc, car, i) => {
      if (car.vin.startsWith(partial)) acc.push(i);
      return acc;
    }, []);

/*
 * we want to assemble a trie that, for each node, will contain the indices of cars with vins that start with the current path.
 * we know vins are 17 chars long, so that helps.
 */
export function assembleCarTrie(cars: Car[]): TrieNode<number> {
  let vin: string;
  const root: TrieNode<number> = createNode(cars.map((_, i) => i))
  for (let carIndex = 0; carIndex < cars.length; carIndex++) {
    ({ vin } = cars[carIndex]);
    insert(vin, carIndex, root);
  }

  return root;
}
