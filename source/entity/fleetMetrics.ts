import { TrieNode, assembleCarTrie, find } from "./Trie";
import Car from "./car";

export interface FleetMetrics {
  purchaseTotal: number;
  countByYear: { [year in number]: number };
}

export const fromCars = <Claim = {}>(cars: Car<Claim>[]): FleetMetrics => {
  return cars.reduce(
    (acc, car) => {
      const yearTotal = acc.countByYear[car.year] || 0;
      acc.countByYear[car.year] = yearTotal + 1;
      acc.purchaseTotal += car.purchasePrice;
      return acc;
    },
    {
      purchaseTotal: 0.0,
      countByYear: {},
    },
  );
};

export const { evict, primeCache, vinLookup } = (() => {
  let lastCars: Car[] | null = null;
  let vinCache: TrieNode<number> | null = null;

  const vinLookup = (vin: string, cars: Car[]) => {
    if (cars !== lastCars || !vinCache) {
      primeCache(cars);
    }

    return find(vin, vinCache!);
  };

  const evict = () => {
    lastCars = null;
    vinCache = null;
  };

  const primeCache = (cars: Car[]) => {
    lastCars = cars;
    vinCache = assembleCarTrie(cars);
  };

  return {
    evict,
    primeCache,
    vinLookup,
  };
})();
