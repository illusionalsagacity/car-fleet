import {
  createNode,
  insert,
  find,
  assembleCarTrie,
  getPartialMatchIndices,
} from "../Trie";

import exampleCars from "../../exampleCars";

describe("Trie", () => {
  describe("createNode", () => {
    const node = createNode(null);

    it("should create a node", () => {
      expect(node).toBeDefined();
    });

    it("should be shaped like a node", () => {
      expect(node).toStrictEqual({
        value: new Set(),
        nodes: new Map(),
      });
    });
  });

  describe("find", () => {
    const leafA1 = createNode("a1");
    const leafB1 = createNode("b1");
    const leafC1 = createNode("c1");

    const nodeA = createNode("a", new Map([["1", leafA1]]));
    const nodeB = createNode("b", new Map([["1", leafB1]]));
    const nodeC = createNode("c", new Map([["1", leafC1]]));

    const trie = createNode(
      null,
      new Map([["a", nodeA], ["b", nodeB], ["c", nodeC]]),
    );

    it("should return null if there is no search string", () => {
      expect(find("", trie)).toEqual(undefined);
    });

    it("should return null if there is no match", () => {
      expect(find("d1", trie)).toEqual(undefined);
    });

    it("should find one level", () => {
      const child = find("a", trie);
      expect(child).toStrictEqual(nodeA);
    });

    it("should find multiple levels", () => {
      const child = find("a1", trie);
      expect(child).toStrictEqual(leafA1);
    });
  });

  describe("insert", () => {
    it("should insert a node at the top level for an empty trie", () => {
      const trie = createNode([]);

      expect(trie.nodes.size).toEqual(0);

      insert("a", "a", trie);

      expect(trie.nodes.size).toEqual(1);
      const child = trie.nodes.get("a");
      expect(child).toBeDefined();
      expect(child).toStrictEqual({
        value: new Set(["a"]),
        nodes: new Map(),
      });
    });

    it("should insert a node at the second level for an empty trie", () => {
      const trie = createNode([]);

      expect(trie.nodes.size).toEqual(0);

      insert("a1", "a", trie);

      expect(trie.nodes.size).toEqual(1);
      const child = trie.nodes.get("a");
      expect(child).toBeDefined();

      const leaf = child.nodes.get("1");
      expect(leaf).toBeDefined();
      expect(leaf).toStrictEqual({
        value: new Set(["a"]),
        nodes: new Map(),
      });
    });

    it("should insert into an existing trie at the top level", () => {
      const nodeA = createNode(["a"]);
      const nodeB = createNode(["b"]);
      const nodeC = createNode(["c"]);

      const trie = createNode(
        [],
        new Map([["a", nodeA], ["b", nodeB], ["c", nodeC]]),
      );

      const insertedValue = "foo";

      expect(trie.nodes.size).toEqual(3);
      expect(nodeA.nodes.size).toEqual(0);
      expect(nodeB.nodes.size).toEqual(0);
      expect(nodeC.nodes.size).toEqual(0);
      insert("a1", insertedValue, trie);
      expect(trie.nodes.size).toEqual(3);
      expect(nodeA.nodes.size).toEqual(1); // value inserted here
      expect(nodeB.nodes.size).toEqual(0);
      expect(nodeC.nodes.size).toEqual(0);

      const insertedNode = nodeA.nodes.get("1");
      expect(insertedNode).toBeDefined();
      expect(insertedNode).toStrictEqual({
        value: new Set([insertedValue]),
        nodes: new Map(),
      });
    });

    it("should insert into an existing trie at multiple levels", () => {
      const leafA1 = createNode(["a1"]);
      const leafA2 = createNode(["a2"]);
      const leafA3 = createNode(["a3"]);

      const nodeA = createNode(
        ["a"],
        new Map([["1", leafA1], ["2", leafA2], ["3", leafA3]]),
      );

      const trie = createNode([], new Map([["a", nodeA]]));

      const insertedValue = "foo";

      expect(trie.nodes.size).toEqual(1);
      expect(nodeA.nodes.size).toEqual(3);
      insert("a11", insertedValue, trie);
      expect(trie.nodes.size).toEqual(1);
      expect(nodeA.nodes.size).toEqual(3);

      const child = nodeA.nodes.get("1");
      expect(child).toBeDefined();

      const inserted = child.nodes.get("1");
      expect(inserted).toBeDefined();
      expect(inserted).toStrictEqual({
        value: new Set([insertedValue]),
        nodes: new Map(),
      });
    });
  });

  describe("assembleCarTrie", () => {
    const testCars = exampleCars;

    it("should assemble a trie", () => {
      const trie = assembleCarTrie(testCars);
      const resultIndex = testCars.findIndex(
        car => car.vin === "WP0CA2A88EK490399",
      );
      expect(resultIndex).not.toEqual(-1);
      const carNode = find("WP0CA2A88EK490399", trie);
      expect(carNode).toBeTruthy();
      expect(carNode).toStrictEqual({
        value: new Set(getPartialMatchIndices("WP0CA2A88EK490399", testCars)),
        nodes: new Map(),
      });
    });
  });
});
