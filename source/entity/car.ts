export default interface Car<Claim = {}> {
  claimHistory: Claim[];
  make: string;
  model: string;
  plateNumber: string;
  purchasePrice: number;
  state: string;
  vin: string;
  year: number;
}
