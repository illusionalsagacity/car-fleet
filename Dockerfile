FROM node:12-alpine as build
RUN apk add --no-cache yarn
COPY ./yarn.lock \
  ./package.json \
  /home/node/
WORKDIR /home/node
RUN yarn install --force
COPY ./source source
COPY ./tsconfig.json \
  ./webpack.config.js \
  ./tailwind.config.js \
  ./postcss.config.js \
  ./babel.config.js \
  /home/node/
WORKDIR /home/node
RUN yarn run build

FROM joshix/caddy as runner
COPY --from=build /home/node/public /var/www/html
COPY ./Caddyfile /var/www/html/Caddyfile
EXPOSE 5000 5001
