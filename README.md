# Car Fleet Coding Challenger

## Installation

Requires the following software:

- [Node 12](https://nodejs.org/en/)
- (OPTIONAL) [Docker Desktop](https://www.docker.com/products/docker-desktop)

To install;

1. clone this git repository or download the code from the zip file in gitlab
1. run `npm i` or `yarn` to install dependencies
1. run `npm run start` or `yarn start` to start a local webserver on port 8080

Alternatively if want to use docker and the production image:

1. `docker build -t car-fleet:latest .`
1. `docker run -p 5000:5000 car-fleet:latest`
