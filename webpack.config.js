const path = require("path");

const AssetConfigPlugin = require("asset-config-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ScriptExtHtmlWebpackPlugin = require("script-ext-html-webpack-plugin");
const CspHtmlWebpackPlugin = require("csp-html-webpack-plugin");
const webpack = require("webpack");

module.exports = (env = {}, argv = {}) => {
  return {
    mode: "development",

    devtool: "cheap-module-eval-source-map",

    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      open: false,
    },

    entry: {
      app: [path.join(__dirname, "source", "index")],
    },

    output: {
      chunkFilename: "[id]_[contenthash].js",
      filename: "[name]_[hash].js",
      path: path.join(__dirname, "public"),
    },

    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loaders: ["babel-loader"],
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            "style-loader",
            { loader: "css-loader", options: { importLoaders: 1 } },
            "postcss-loader",
          ],
        },
      ],
    },

    plugins: [
      new webpack.EnvironmentPlugin({
        NODE_ENV: "development",
        DEBUG: false,
      }),
      new AssetConfigPlugin(),
      new HtmlWebpackPlugin({
        template: path.join(__dirname, "source", "index.html"),
        inject: true,
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: "async",
      }),
      new CspHtmlWebpackPlugin(),
    ],

    resolve: {
      alias: {
        // "react-dom": "@hot-loader/react-dom",
      },
      extensions: [".wasm", ".mjs", ".js", ".json", ".ts", ".tsx"],
    },
  };
};
